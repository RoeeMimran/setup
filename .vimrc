syntax on

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set ignorecase smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set hlsearch
set wildmenu
highlight ColorColumn ctermbg=0 guibg=lightgrey

let g:python_highlight_all = 1
let  g:C_UseTool_cmake    = 'yes'

" assumes set ignorecase smartcase
augroup dynamic_smartcase
    autocmd!
    autocmd CmdLineEnter : set nosmartcase
    autocmd CmdLineLeave : set smartcase
augroup END

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'leafgarland/typescript-vim'
Plug 'vim-utils/vim-man'
Plug 'kien/ctrlp.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'mbbill/undotree'
Plug 'vim-scripts/c.vim'
Plug 'scrooloose/nerdtree'
Plug 'vhdirk/vim-cmake'
Plug 'vim-python/python-syntax'

call plug#end()

colorscheme gruvbox
set background=dark

:nnoremap <F5> :NERDTree<cr>
:nnoremap <F6> :UndotreeToggle<CR>

:nnoremap <Space>d :wincmd v<cr> :wincmd l<cr>
:nnoremap <Space>s :wincmd s<cr> :wincmd j<cr>
:nnoremap <Space>w :wincmd n<cr> :wincmd k<cr>

:nnoremap <Space><right> :wincmd l<cr>
:nnoremap <Space><up> :wincmd k<cr>
:nnoremap <Space><down> :wincmd j<cr>
:nnoremap <Space><left> :wincmd h<cr>
